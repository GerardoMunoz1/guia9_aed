#include <iostream>
#include <stdlib.h>
#include "Hash_creator.h"

using namespace std;

void menu(string metodo){

    string n;
    string opcion;
    string numero;
    int clave;
    string aux;

    cout << "Ingrese un valor numérico para el tamaño del arreglo: ";
    getline(cin, n);

    Hash_creator generador = Hash_creator(metodo, stoi(n));
    int array[stoi(n)] = {};

    system("clear");

    while(1){
        cout << "\t== MENU PRINCIPAL ==" << endl;
        cout << "\n[1] Ingresar un elemento al arreglo." << endl;
        cout << "[2] Buscar un elemento en el arreglo." << endl;
        cout << "[3] Salir." << endl;
        cout << "OPCION: ";
        getline(cin, opcion);

        if (opcion == "1"){

            cout << "\n> INGRESANDO ELEMENTO...";
            cout << "\nIngrese número para añadir al arreglo: ";
            getline(cin, numero);

            clave = generador.generar_hash(stoi(n), stoi(numero), array, 0);

            if(clave != -1){
                array[clave] = stoi(numero);
            }
            else{
                cout << "Error, el arreglo se encuentra lleno." << endl;
            }

            cout << "\n_____________________________________________" << endl;
            cout << "> IMPRIMIENDO ARREGLO ACTUAL...\n" << endl;
            cout << "       [ Posicion | Valor ]" << endl;
            cout << "       --------------------" << endl;

            for(int i=0; i<stoi(n); i++){
                cout << "\t\t";
                cout << i+1 << " | " << array[i] << endl;
            }

            cout << "\n> Presione enter para continuar...." << endl;
            getline(cin, aux);
            system("clear");
        }
        else if(opcion == "2"){
            cout << "\n> BUSCANDO ELEMENTO...";
            cout << "\nIngrese número para buscar en el arreglo: ";
            getline(cin, numero);

            generador.generar_hash(stoi(n), stoi(numero), array, 1);
        }
        else if(opcion == "3"){
            cout << "\nHasta pronto!" << endl;
            exit(0);
        }
        else{
            cout << "Opción no válida, inténtelo nuevamente." << endl;
            system("sleep 1.5");
            system("clear");
        }
    }
}

int main(int argc, char **argv){

    string param;

    if (argc != 2){
        cout << "Error. Debe ingresar solo 1 parámetro." << endl;
    }
    else{
        param = argv[1];

        if(param != "l" && param != "c" && param != "d" && param != "e"){
            cout << "\nParámetro incorrecto. Parámetros disponibles: { l | c | d | e }." << endl;
            exit(1);
        }

        // Se hizo la comprobación de entrada, si llega a este punto
        // significa que si ha ingresado un parámetro válido. Se da la bienvenida
        // con un título, mencionando además que opción ha escogido.
        cout << "\n\t== Métodos de Búsqueda ==\n" << endl;

        if(param == "l") {
            cout << "Haz escogido el método <Reasignación Prueba Lineal (L)>" << endl;
            menu(param);
        }
        else if(param == "c") {
            cout << "Haz escogido el método <Reasignación Prueba Cuadrática (C)>" << endl;
            menu(param);
        }
        else if(param == "d") {
            cout << "Haz escogido el método <Reasignación Doble Dirección Hash (D)>" << endl;
            menu(param);
        }
        else if(param == "e") {
            cout << "Haz escogido el método <Encadenamiento (E)>" << endl;
            menu(param);
        }
        else{
            cout << "Error. Inténtelo nuevamente." << endl;
        }
    }
    return 0;
}
