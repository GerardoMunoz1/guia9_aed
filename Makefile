prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = Main.cpp Hash_creator.cpp
OBJ = Main.o Hash_creator.o
APP = hash

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
