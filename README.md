# Guía 9 - Métodos de Búsqueda - Tablas Hash

## Ingeniería Civil en Bioinformática - Universidad de Talca

### Pre-requisitos

Para ejecutar correctamente el programa, se debe tener instalado un compilador de C++.

### Instalación

Para instalar el programa, se debe descargar el presente repositorio en una carpeta existente o no. 

```
$ cd carpeta_existente
```
Dentro de la carpeta, ejecutamos el archivo MakeFile:

```
$ make
```

Para correr correctamente el programa, escribimos en la terminal:

```
$ ./hash [parametro n°1]
```

En donde "parametro n°1" corresponden al modo de uso del programa, siendo el primer parametro el tipo de búsqueda que se desea realizar ( L | C | D | E).

Ejemplo:

```
$ ./hash l
```